# Daily Diary of Karthik Shyamsunder


## Sunday July 25, 2021
- Investigate how this max function works:

    ```python
    def find_winner(**kwargs):
        return max(kwargs, key = kwargs.get)

    print(find_winner(Andy = 17, Marry = 19, Sim = 45, Kae = 34))
    ```
    - It looks like what get's passed to `max` function is a `dict` that looks like this `{'Andy': 17, 'Marry':19, 'Sim':45, 'Kae':34}`, but then how does it work??? #TODO

## Friday July 23, 2021

### VSCode
- In the search extensions, you can view extensions in many way by clicking @ 
- Change Python interpreter by clicking on Python version on left below on the status bar which will being a popup of Python versions, then simply select one
- Command+Shift+p will open the command pallette`


### Figuring out python version and location
- Using sys module we can figure what version of python we have and where it is located
    ```python
    import sys
    print (sys.version)
    print (sys.executable)
    ```
### Difference between Python print and pprint
- https://amalgjose.com/2021/05/09/what-is-the-difference-between-print-and-pprint-in-python/
- Here is a demonstration of a complex json
    ```python
    from pprint import pprint

    sample_json = {
        "id": "0001",
        "type": "donut",
        "name": "Cake",
        "ppu": 0.55,
        "batters":
            {
                "batter":
                    [
                        { "id": "1001", "type": "Regular" },
                        { "id": "1002", "type": "Chocolate" },
                        { "id": "1003", "type": "Blueberry" },
                        { "id": "1004", "type": "Devil's Food" }
                    ]
            },
        "topping":
            [
                { "id": "5001", "type": "None" },
                { "id": "5002", "type": "Glazed" },
                { "id": "5005", "type": "Sugar" },
                { "id": "5007", "type": "Powdered Sugar" },
                { "id": "5006", "type": "Chocolate with Sprinkles" },
                { "id": "5003", "type": "Chocolate" },
                { "id": "5004", "type": "Maple" }
            ]
    }

    print(sample_json)
    pprint(sample_json)
    ```
- pprint prints attributes in alphabetical order and also formats based on complexity
 
### JSON Web Token 
- Proposed Internet standard for creating data with optional signature and/or optional encryption whose payload holds JSON that asserts some number of claims.
- Used by Google Voice Assistant to share name, username and profile picture


### Print without a new line in Python 
- `print("Hello there!", end = '')` 

### Python String formatting options explained
- https://blog.finxter.com/string-formatting-vs-format-vs-formatted-string-literal/

### Indian company names list
- https://business.mapsofindia.com/india-company/#a

### FreeCodeCamp.org course on API
- https://www.youtube.com/watch?v=GZvSYJDk-us

### Differencs  between .gitignore and .gitkeep
- Git cannot add a completely empty directory. People who want to track empty directories in Git have created the convention of putting files called .gitkeep in these directories. The file could be called anything; Git assigns no special significance to this name.
- .gitignore is  used to list files that should be ignored by Git when looking for untracked files.
- There is a competing convention of adding a .gitignore file to the empty directories to get them tracked, but some people see this as confusing since the goal is to keep the empty directories, not ignore them
- If you want an empty directory and make sure it stays 'clean' for Git, create a .gitignore containing the following lines within:
    ```code
    # .gitignore sample
    ###################

    # Ignore all files in this dir...
    *

    # ... except for this one.
    !.gitignore
    ```
- If you desire to have only one type of files being visible to Git, here is an example how to filter everything out, except .gitignore and all .txt files:

    ```code
    # .gitignore to keep just .txt files
    ###################################

    # Filter everything...
    *

    # ... except the .gitignore...
    !.gitignore

    # ... and all text files.
!*.txt
    ```



### RICE Scoring model for prioritization
- https://www.productplan.com/glossary/rice-scoring-model/
- Reach Impact Confidence and Effort

### Meditation 
- Yongey Mingyur Rinpoche has several talks on meditation
- Meditation is awareness
- If you are aware, panic comes, you are aware so nothing to be afraid
- Treat panic and anxiety as friends

### Gitignore
- You can have  a file called .gitignore in which you list all the files that you wangt git to ignore
- You can create .ignore for a language from gitignore.io which redirects to https://www.toptal.com/developers/gitignore

### Cloudbees
- Platform/Managed service of Jenkins

### Multi root workspaces in VS code
- A good video that shows how to do that https://www.youtube.com/watch?v=2yOQUtP_GcY
- Documentation of multi-root wordspace https://code.visualstudio.com/docs/editor/workspaces#_multiroot-workspaces

### Data companies
- https://docs.peopledatalabs.com  
    - https://docs.peopledatalabs.com/docs/datasets has list of of all datasets 
    https://docs.peopledatalabs.com/docs/company-search-api is free
- https://cloud.google.com/bigquery/public-data
- Google research dataset search https://datasetsearch.research.google.com/
- https://www.dataandsons.com/
- https://datastock.shop/

### mydukaan
- Has become like shopify.com
- Format of a page is mydukaan.com/handlename
  
### Git pull push
- `$ git push` # pushes to origin
- `$ git push origin` # same as `git push`
- `$ git push branchname` # pushes branchname
- After pushing, you can do a pull request
- Also do a git pull befire pushing so you can resolve conflicts
- Many IDEs actually do a git pull, before a git push




## Thursday July 22, 2021
### Google Assistant
- In Amazon Alexa, you build Skills
- In Google Assistant, you build Actions
- The apps you create in Google assistant are called "Actions" which is used tio extend Google Assistant
- The Google assistant and the apps talk directly
- Actions protocol is in Jason Format
- Access Google Assistant at https://developers.google.com/assistant
- Access console at https://console.developers.google.com
- You can create actions in many languages
- Google  provides templates for certain kinds of applications like quiz which is connected to Google Sheets, but it is limited in functionality
- Google provides SDK also
- Google also provides a simulator
- Google provides DialogFlow, a service for natural language processing
- An Intent is a functionality that the user wants to achieve.  For example an "Get a job" would have all the phrases a user could say to recognize the intent.  Another intent example would be to hear a joke, which means all the phrases the user could say such as "Hear a joke", "Tell me something funny" etc.
- Dialogflow is r   ebranded version of api.ai
- Google Actions Flow
 ![Google Actions Flow](images/google-actions-flow.png)


### Node.js
- A video that shows best courses on Node.js on udemy.com https://www.youtube.com/watch?v=I45zGq5nubQ

### Jovo
- jovo.tech is the official site 
Jovo is a platform that allows you to build cross platform voice applications
- Platforms include Amazon Alexa, Google Assistant, mobile phones, web applications, Rasberry Pi and more
- Claims to be open source
- Jovo has a simulator so you don't have to upload to any cloud provider, instead test the conversation locally 
- Build applications using Node.js and JavaScript
- Lots of tutorials in jovo.tech and Youtube




